﻿Shader "Lighthouse/ScrollUV" {
	Properties {
		_Color ("Diffuse Tint", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ScrollXSpeed ("X Scroll Speed", Range(0,10)) = 2
		_ScrollYSpeed ("Y Scroll Speed", Range(0,10)) = 2
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM		
		#pragma surface surf Standard fullforwardshadows		
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;

		struct Input {
			float2 uv_MainTex;
		};
				

		void surf (Input IN, inout SurfaceOutputStandard o) {				
			fixed2 scrolledUV = IN.uv_MainTex;

			// Create variables that store the individual x and y components for the UV's scaled by time			
			fixed xScrollValue = _ScrollXSpeed * _Time;
			fixed yScrollValue = _ScrollYSpeed * _Time;

			// Apply the final UV offset
			scrolledUV += fixed2(xScrollValue, yScrollValue);
						
			half4 c = tex2D(_MainTex, scrolledUV);
			o.Albedo = c.rgb * _Color;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
