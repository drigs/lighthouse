﻿using System.Collections;
using System.Collections.Generic;
using ChronosFramework;
using ChronosFramework.FirstPersonController;
using UnityEngine;

public class BreakableObject : Trigger
{
    public ParticleSystem particlesPrefab;

    private void Start()
    {
        repeatable = true;
    }
    
    public void Break()
    {
        FirstPersonController.Instance.TweakCrosshairSize(Vector3.one);

        Tween scaleObject = new Tween(transform.localScale, transform.localScale * 1.5f, .5f, true, EasingType.EaseIn, (scale) =>
        {
            transform.localScale = scale;
        });

        Material material = GetComponentInChildren<Renderer>().material;
        material.SetRenderingMode(RenderingMode.Fade);

        Tween fadeObject = material.FadeOut(.5f);

        particlesPrefab = Instantiate(particlesPrefab) as ParticleSystem;
        particlesPrefab.transform.position = transform.position;

        ParticleSystem.ShapeModule shape = particlesPrefab.shape;
        var collider = GetComponent<Collider>();
        shape.box = collider.bounds.size;

        if (collider is CapsuleCollider && ((CapsuleCollider)collider).direction == 1)
        {
            var capsule = ((CapsuleCollider)collider);
            shape.box = new Vector3(capsule.radius, capsule.radius, capsule.height);
        }

        particlesPrefab.Play();

        Timer timer = new Timer(particlesPrefab.main.duration, particlesPrefab.Stop);

        Destroy(collider);
        Destroy(gameObject, 1f);

        enabled = false;
    }
}
