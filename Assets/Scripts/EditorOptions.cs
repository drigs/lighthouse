﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class EditorOptions : ChronosFramework.Singleton<EditorOptions>
{
    public UnityEvent preLoadActions;
    public GameObject[] doNotLoad;
    
    void Awake()
    {
        preLoadActions.Invoke();

        foreach (var obj in doNotLoad)
        {
            obj.SetActive(false);
        }
    }
}
#endif
