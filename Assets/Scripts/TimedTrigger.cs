﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using ChronosFramework;

public class TimedTrigger : MonoBehaviour
{
    public float duration = 1;
    public bool startOnAwake = true;
    public UnityEvent onComplete;

    void Start()
    {
        if (startOnAwake)
        {
            StartTimer();
        }
    }

    void StartTimer()
    {
        Timer timer = new Timer(duration, onComplete.Invoke);
    }
}
