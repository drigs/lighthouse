﻿using System.Linq;
using System.Collections.Generic;
using ChronosFramework;
using ChronosFramework.FirstPersonController;
using UnityEngine;
using System.Threading;

public class Player : Singleton<Player>
{
    public Transform hand;
    public bool hasAxe { get; private set; }

    WindZone wind;
    
    Vector3 startPosition;

    private void Start()
    {
        startPosition = transform.position;
        wind = GetComponentInChildren<WindZone>();
        wind.gameObject.SetActive(false);

        FirstPersonController fpc = FirstPersonController.Instance;

        fpc.OnJump += () => wind.gameObject.SetActive(true);
        fpc.OnLand += () => wind.gameObject.SetActive(false);
    }

    public void ResetStartPosition()
    {
        startPosition = transform.position;
    }

    public void ReturnToStartPosition()
    {
        transform.position = startPosition;
    }

    public void PickUpAxe(Transform axe)
    {
        FirstPersonController.Instance.TweakCrosshairSize(Vector3.one);

        Destroy(axe.GetComponent<Collider>());

        Tween pickup = new Tween(axe, hand, .5f, true, EasingType.EaseOut, (pos, rot) =>
        {
            axe.position = pos;
            axe.rotation = rot;
        });

        pickup.OnComplete += () =>
        {
            axe.SetParent(hand);
            hasAxe = true;
            axe.GetComponent<Axe>().animator.enabled = true;
        };
    }
}
