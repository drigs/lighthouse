﻿using UnityEngine;
using UnityEngine.UI;
using ChronosFramework;

public class ColorTween : MonoBehaviour {

    public Graphic target;
    public Color startColor = Color.black;
    public Color endColor = Color.clear;
    public float duration = 1;
    public EasingType curve = EasingType.Linear;
    public ulong delay = 0;
    public bool startOnAwake;

	void Start ()
    {
	    if (startOnAwake)
        {
            if (delay > 0)
            {
                Timer timer = new Timer(delay, StartTween);
            }
            else
            {
                StartTween();
            }
        }
	}

    public void StartTween()
    {
        target.color = startColor;
        target.CrossFadeColor(endColor, duration, false, true);
    }
}
