﻿using UnityEngine;
using ChronosFramework;
using UnityEngine.Events;

public class Lighthouse : MonoBehaviour
{
    [SerializeField]MeshRenderer meshTrigger;

    [SerializeField]
    Transform lightPivot;

    [SerializeField]
    Transform water;

    public float speed = 10;
    public UnityEvent onSolve;

    Light spotlight;
    LineRenderer shaft;
    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        shaft = GetComponentInChildren<LineRenderer>();
        spotlight = GetComponentInChildren<Light>();

        UpdateAction Update = null;
        UpdateAction MusicUpdate = null;
        
        Update += MusicUpdate;

        Update += delegate
        {
            lightPivot.Rotate(Vector3.up, Time.deltaTime * speed);
        };

        Update.AddToUpdate();
    }

    public void CheckPassword(UnityEngine.UI.InputField input)
    {
        if (input.text.Equals("Catharsis", System.StringComparison.OrdinalIgnoreCase))
        {
            audioSource.Play();
            Vector3 waterPos = water.transform.position;
            Tween waterTween = new Tween(waterPos, new Vector3(0, 270), 40, false, EasingType.EaseIn, delegate(Vector3 value)
             {
                 water.transform.position = value;
             });

            onSolve.Invoke();

            Destroy(input.gameObject);
        }
    }
}
