﻿using System.Collections;
using System.Collections.Generic;
using ChronosFramework;
using UnityEngine;

public class Axe : MonoBehaviour
{
    public Animator animator { get; private set; }
    public Collider contactPoint;
    public LayerMask collisionMask;

    private void Start()
    {
        animator = GetComponent<Animator>();
        animator.Stop();
    }

    private void Update()
    {
        if (animator.isActiveAndEnabled)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                animator.Play("Swing");
            }
        }
    }

    public void CheckCollision()
    {
        var collisions = Physics.OverlapBox(contactPoint.bounds.center, contactPoint.bounds.extents, contactPoint.transform.rotation, collisionMask);
        if (collisions.Length > 0)
        {
            Debug.Log(collisions[0].name);

            var breakableObj = collisions[0].GetComponent<BreakableObject>();

            if (breakableObj)
            {
                breakableObj.Break();
            }
        }
    }
}
