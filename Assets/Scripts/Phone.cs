﻿using UnityEngine;
using System.Linq;
using ChronosFramework;
using UnityEngine.UI;

public class Phone : Singleton<Phone>
{
    struct PhoneButton
    {
        public Transform button;
        public int number;
        public bool isPressed;
    }

    [SerializeField]Transform buttonsParent;
    [SerializeField]Text[] letterRows;
    PhoneButton[] buttons;

    void Awake()
    {
        Transform[] buttonObjs = buttonsParent.GetComponentsInChildren<Transform>();
        //buttons.Zip(buttonObjs, (button, obj) => obj);

        buttons = new PhoneButton[buttonObjs.Length];

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].button = i + 1 < buttonObjs.Length ? buttonObjs[i + 1] : null;
            buttons[i].number = i;
            buttons[i].isPressed = false;
        }
    }

    public void PressButton(int number)
    {
        if (number > 9 || number < 0)
        {
            return;
        }

        char[] letters = new char[0];

        switch (number)
        {
            case 2:
                letters = new char[] { 'A', 'B', 'C' };
                break;
            case 3:
                letters = new char[] { 'D', 'E', 'F' };
                break;
            case 4:
                letters = new char[] { 'G', 'H', 'I' };
                break;
            case 5:
                letters = new char[] { 'J', 'K', 'L' };
                break;
            case 6:
                letters = new char[] { 'M', 'N', 'O' };
                break;
            case 7:
                letters = new char[] { 'P', 'Q', 'R', 'S' };
                break;
            case 8:
                letters = new char[] { 'T', 'U', 'V' };
                break;
        }

        var button = buttons[number];

        for (int i = 0; i < letters.Length; i++)
        {
            letterRows[i].text += letters[i] + " ";
        }

        if (!button.isPressed)
        {
            Debug.Log(button.isPressed);
            button.isPressed = true;

            var buttonTransform = button.button;

            Tween press = new Tween(buttonTransform.localPosition, buttonTransform.localPosition + Vector3.forward / 5f,
            0.5f, true, EasingType.Bell, delegate (Vector3 pos)
            {
                buttonTransform.localPosition = pos;
            });

            press.OnComplete += delegate
            {
                button.isPressed = false;
                Debug.Log(button.isPressed);
            };

            Debug.Log("Pressed " + buttons[number].number);
        }
    }
}
