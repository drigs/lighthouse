﻿using System.Collections;
using System.Collections.Generic;
using ChronosFramework;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class Follower : MonoBehaviour
{
    public AudioClip noiseClip, beatClip;

    public float speed = 2.5f;
    
    private void Start()
    {
        Vector3 startPosition = transform.position;

        Renderer meshRenderer = GetComponentInChildren<Renderer>();

        AudioSource noise = GetComponent<AudioSource>();

        noise.clip = beatClip;

        UpdateAction update = null; update = () =>
        {
            Vector3 playerPos = Player.Instance.transform.position;
            Vector3 position = transform.position;
            float distance = Vector3.Distance(playerPos, transform.position);

            if (distance < 10f) // <-- Placeholder condition;
            {
                if (!noise.isPlaying)
                {
                    noise.Play();
                }

                noise.volume = 1f / distance;

                if (distance > 2f)
                {
                    Vector3 direction = (playerPos - position).normalized;

                    if (!meshRenderer.isVisible)
                    {
                        transform.Translate(direction * speed * Time.deltaTime, Space.World);

                        position = new Vector3(transform.position.x, position.y, transform.position.z);
                        transform.position = position;

                        transform.LookAt(new Vector3(playerPos.x, position.y, playerPos.z));
                    }
                }
                else
                {
                    noise.PlayOneShot(noiseClip);
                    Player.Instance.ReturnToStartPosition();
                    transform.position = startPosition;
                }
            }
            else if (noise.isPlaying)
                noise.Stop();
        };
        
        //Placeholder: This must be called by a trigger
        update.AddToUpdate();
    }
}
