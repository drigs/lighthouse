﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ChronosFramework.EventSystem
{
    public class GameEventQueue
    {
        Queue<GameEvent> eventQueue = new Queue<GameEvent>();
        
        public void Enqueue(GameEvent gameEvent)
        {
            eventQueue.Enqueue(gameEvent);
        }

        public bool TriggerEvent()
        {
            var nextEvent = eventQueue.Peek();

            if (nextEvent.triggerCondition())
            {
                nextEvent.onTrigger();
                eventQueue.Dequeue();
                return true;
            }

            return false;
        }
    }
}
