﻿using UnityEngine;
using System.Collections;

namespace ChronosFramework
{
    public class Fader : MonoBehaviour
    {
        public Renderer target;
        public FadeType fadeType;
        public RenderingMode transparency;
        public float delay;
        public float duration = 1;
        public bool beginOnStart = true;

        Tween fadeTween;

        void Start()
        {
            if (beginOnStart)
            {
                StartFade();
            }
        }

        public void StartFade()
        {
            if (delay > 0)
            {
                Timer delayTimer = new Timer(duration, Fade);
            }
            else
            {
                Fade();
            }
        }

        void Fade()
        {
            target.material.SetRenderingMode(transparency);

            switch (fadeType)
            {
                case FadeType.FadeIn:
                    fadeTween = target.material.FadeIn(duration);
                    break;
                case FadeType.FadeOut:
                    fadeTween = target.material.FadeOut(duration);
                    break;
                case FadeType.FadeInOut:
                    fadeTween = target.material.FadeIn(duration / 2f);
                    fadeTween.OnComplete += () =>
                    {
                        fadeTween = target.material.FadeOut(duration / 2f);
                    };
                    break;
                default:
                    break;
            }
        }
    }
}
