﻿using UnityEngine;
using System.Collections;

namespace ChronosFramework
{
    public class Timer
    {
        public event UpdateAction OnComplete;

        public bool isLoop { get; set; }

        UpdateAction Update;

        float duration;

        float elapsedTime;

        public Timer(float duration, bool isThread, UpdateAction callback)
        {
            this.duration = duration;

            OnComplete += callback;

            if (isThread)
            {
                CoreManager.Instance.StartCoroutine(ThreadTimer());
            }
            else
            {
                Update = delegate
                {
                    elapsedTime += Time.deltaTime;

                    if (elapsedTime >= duration)
                    {
                        OnComplete();

                        if (isLoop)
                        {
                            elapsedTime = 0;
                        }
                        else
                        {
                            Update.RemoveFromUpdate();
                        }
                    }
                };

                Update.AddToUpdate();
            }
        }

        public Timer(float duration, UpdateAction callback)
        {
            this.duration = duration;

            OnComplete += callback;
            
            Update = delegate
            {
                elapsedTime += Time.deltaTime;

                if (elapsedTime >= duration)
                {
                    OnComplete();

                    if (isLoop)
                    {
                        elapsedTime = 0;
                    }
                    else
                    {
                        Update.RemoveFromUpdate();
                    }
                }
            };

            Update.AddToUpdate();
        }

        public void Start()
        {
            Update.AddToUpdate();
        }

        public void Pause()
        {
            Update.RemoveFromUpdate();
        }

        public void Stop()
        {
            Update.RemoveFromUpdate();

            elapsedTime = 0;
        }

        IEnumerator ThreadTimer()
        {
            yield return new WaitForSeconds(duration);

            OnComplete();

            if (isLoop)
            {
                CoreManager.Instance.StartCoroutine(ThreadTimer());
            }
        }
    }
}
