﻿using UnityEngine;
using System.Collections;
using System;

namespace ChronosFramework.FirstPersonController
{
    public abstract class Highlightable : InteractiveObject
    {
        protected bool isHighlighted { get; private set; }

        public bool highlightable { get; set; }

        protected void Awake()
        {
            highlightable = true;

            gameObject.layer = LayerMask.NameToLayer("Interactive");
        }

        void Update()
        {
            if (isHighlighted)
            {
                if (!FirstPersonController.Instance.CheckInteractionDistance(transform.position))
                {
                    Unhighlight();
                }
            }
        }

        protected void Highlight()
        {
            isHighlighted = true;
            FirstPersonController.Instance.TweakCrosshairSize(new Vector2(3, 3));
        }

        protected void Unhighlight()
        {
            isHighlighted = false;
        }

        public override void OnMouseEnter()
        {
            if (enabled)
                if (isActiveAndEnabled)
                {
                    if (highlightable &&
                    Vector3.Distance(transform.position, FirstPersonController.Instance.transform.position) <= FirstPersonController.Instance.interactionDistance * 2)
                    {
                        if (FirstPersonController.Instance.CheckInteractionDistance(transform.position))
                        {
                            if (!isHighlighted)
                            {
                                Highlight();
                            }
                        }
                        else
                        {
                            FirstPersonController.Instance.TweakCrosshairSize(new Vector2(2, 2));
                        }
                    }
                }
        }

        public override void OnMouseOver()
        {
            if (enabled)
                if (isActiveAndEnabled)
                {
                    if (highlightable &&
                    Vector3.Distance(transform.position, FirstPersonController.Instance.transform.position) <= FirstPersonController.Instance.interactionDistance * 2)
                    {
                        if (FirstPersonController.Instance.CheckInteractionDistance(transform.position))
                        {
                            if (!isHighlighted)
                            {
                                Highlight();
                            }
                        }
                        else
                        {
                            FirstPersonController.Instance.TweakCrosshairSize(new Vector2(2, 2));
                        }
                    }
                }
        }

        public override void OnMouseExit()
        {
            FirstPersonController.Instance.TweakCrosshairSize(new Vector2(1, 1));

            if (isHighlighted)
            {
                Unhighlight();
            }
        }

        void OnDisable()
        {
            Unhighlight();
        }
    }
}
