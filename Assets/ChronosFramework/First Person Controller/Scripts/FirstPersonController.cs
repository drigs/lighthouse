﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ChronosFramework.FirstPersonController
{
    public class FirstPersonController : Singleton<FirstPersonController>
    {
        #region References

        [SerializeField]
        AudioClip[] footsteps;
        [SerializeField]
        Image crosshair;
        [SerializeField]
        LayerMask interactionMask;

        #endregion

        #region Attributes
        public Image Crosshair { get { return crosshair; } }

        public float radius { get; private set; }
        public float height { get; private set; }
        public float speed = 9.6f;
        public float turnSpeed = 2f;
        public float interactionDistance = 5f;
        public float zoomAmount = 25;
		public float jumpForce = 10f;
        
        public bool isFreeToInteract { get { return canRotate; } }

        public RectTransform mainCanvas;
        public Transform[] cameraPoints;

        public event UpdateAction OnJump;
        public event UpdateAction OnLand;

        float initialFov;
        float stepCycle;
        float nextStep;
        
        bool canMove = true;
        bool canRotate = true;
        bool canUnlock = true;
        bool zooming;
        bool jumping;

        CharacterController cc;
        Rigidbody rb;

        BoxCollider boxChecker;

        AudioSource audioSource;

        Vector2 crosshairSize;
        Vector2 currentCrosshairSize;
        #endregion

        #region Methods
        void Initialize()
        {
            audioSource = GetComponent<AudioSource>();
            cc = GetComponent<CharacterController>();
            rb = GetComponent<Rigidbody>();

            radius = cc.radius;
            height = cc.height;

            initialFov = Camera.main.fieldOfView;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            crosshair.color = Color.gray;

            currentCrosshairSize = crosshairSize = crosshair.rectTransform.sizeDelta;

            TweakCrosshairSize(Vector2.one);
        }

        bool CheckEndJump(ref Vector3 currentJumpForce)
        {
            currentJumpForce += Physics.gravity * Time.fixedDeltaTime;

            jumping = (currentJumpForce.y > .5f && (currentJumpForce.y < jumpForce / 2 ? Input.GetButton("Jump") : true));
            
            return jumping;
        }

        void Awake()
        {
            Initialize();
        }

        void FixedUpdate()
        {
            if (cc.enabled && !cc.isGrounded && !jumping)
            {
                cc.Move(Physics.gravity * Time.fixedDeltaTime);

                if (cc.isGrounded)
                {
                    OnLand();
                }
            }

            if (canMove)
            {
                float xMove = Input.GetAxisRaw("Horizontal");
                float zMove = Input.GetAxisRaw("Vertical");

                if (cc.isGrounded)
                {                   
                    //Grounded movement
                    if (Mathf.Abs(xMove) > 0 || Mathf.Abs(zMove) > 0)
                    {
                        Vector3 movement = (transform.right * xMove + transform.forward * zMove).normalized;

                        RaycastHit hitInfo;
                        if (Physics.SphereCast(transform.position, radius, Vector3.down, out hitInfo, height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore))
                        {
                            movement = Vector3.ProjectOnPlane(movement, hitInfo.normal).normalized;

                            Vector3 direction = movement.normalized;

                            movement = direction * speed * Time.fixedDeltaTime;

                            Walk(movement);
                        }
                    }
                }
                //Jump movement
                else
                {
                    Vector3 movement = (transform.right * xMove + transform.forward * zMove).normalized;
                    Vector3 direction = movement.normalized;
                    movement = direction * speed * Time.fixedDeltaTime;
                    cc.Move(movement);
                }
            }
        }

        void Update()
        {
            if (!jumping)
            {
                if (Input.GetButtonDown("Jump"))
                {
                    if (Physics.Raycast(transform.position, Vector3.down, cc.height / 1.5f))
                    {
                        jumping = true;

                        Vector3 currentJumpForce = Vector3.up * jumpForce;

                        EventManager.WhileFixed(() => cc.Move(currentJumpForce * 2 * Time.fixedDeltaTime), () => CheckEndJump(ref currentJumpForce));

                        OnJump();
                    }
                    else
                    {
                        Debug.LogWarning("Can't jump because player isn't grounded!");
                    }
                }
            }

            if (canRotate)
            {
                //Rotation
                float xRot = Input.GetAxis("Mouse Y") * turnSpeed;
                float yRot = Input.GetAxis("Mouse X") * turnSpeed;

                Quaternion camRot0 = Camera.main.transform.localRotation;
                Quaternion playerRot0 = transform.localRotation;

                Quaternion cameraRot = Camera.main.transform.localRotation * Quaternion.Euler(-xRot, 0, 0);
                Quaternion playerRot = transform.localRotation * Quaternion.Euler(0, yRot, 0);

                cameraRot = ClampRotationAroundXAxis(cameraRot);

                transform.localRotation = playerRot;
                Camera.main.transform.localRotation = cameraRot;
            }

			CheckInput();
        }

        public void ToggleCursorLock()
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.Confined : CursorLockMode.Locked;
        }

        public void SetLock(bool value)
        {
            canUnlock = !value;
        }

        public void SetMove(bool value)
        {
            cc.enabled = canMove = value;
        }

        public void SetRotate(bool value)
        {
            canRotate = value;
            mainCanvas.gameObject.SetActive(value);
        }

        void Walk(Vector3 movement)
        {
            Vector3 previousPos = transform.position;

            cc.Move(movement);

            stepCycle += (cc.velocity.magnitude + speed) * Time.fixedDeltaTime;

            if (Vector3.Distance(previousPos, transform.position) > .05f)
            {
                if (!(stepCycle > nextStep))
                {
                    return;
                }

                nextStep = stepCycle + 3.5f;

                int n = UnityEngine.Random.Range(1, footsteps.Length);

                audioSource.clip = footsteps[n];
                audioSource.pitch = UnityEngine.Random.Range(.85f, 1.15f);
                audioSource.PlayOneShot(audioSource.clip);

                footsteps[n] = footsteps[0];
                footsteps[0] = audioSource.clip;
            }
        }

        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, -60, 60);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }
        
        void CheckInput()
        {
            if (isFreeToInteract)
            {
                if (!Input.GetButton("Fire2"))
                {
                    if (zooming)
                    {
                        Tween cameraZoom = new Tween(Camera.main.fieldOfView, initialFov, .25f, false, EasingType.EaseOut, (value) =>
                            Camera.main.fieldOfView = value);
                        
                        zooming = false;
                    }
                }
                else
                {
                    if (!zooming)
                    {
                        Tween cameraZoom = new Tween(Camera.main.fieldOfView, initialFov - 25, .25f, false, EasingType.EaseOut, (value) =>
                            Camera.main.fieldOfView = value);

                        zooming = true;
                    }
                }
            }     
        }

        public void ToggleMove()
        {
            SetMove(!canMove);
        }

        public bool CheckInteractionDistance(Vector3 target)
        {
            return Vector3.Distance(transform.position, target) <= interactionDistance;
        }

        public void TweakCrosshairSize(Vector2 newSize)
        {
            if (currentCrosshairSize != newSize)
            {
                currentCrosshairSize = newSize;

                newSize = new Vector2(crosshairSize.x * newSize.x, crosshairSize.y * newSize.y);

                Tween sizeTween = new Tween(crosshair.rectTransform.sizeDelta, newSize, .2f, false, EasingType.EaseOut, delegate (Vector3 size)
                {
                    crosshair.rectTransform.sizeDelta = size;
                });

                if (newSize.x > crosshairSize.x * 2)
                {
                    Tween colorTween = new Tween(crosshair.color, Color.white, .2f, false, EasingType.EaseOut, delegate (Color color)
                    {
                        crosshair.color = color;
                    });
                }
                else
                {
                    Tween colorTween = new Tween(crosshair.color, Color.gray, .2f, false, EasingType.EaseOut, delegate (Color color)
                    {
                        crosshair.color = color;
                    });
                }
            }
        }
        #endregion
    }
}